package com.googlemap

import android.content.Context
import android.location.LocationManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterManager
import com.googlemap.model.Person


class MainActivity : AppCompatActivity(), OnMapReadyCallback {

    lateinit var mLocationManager: LocationManager
    lateinit var mClusterManager: ClusterManager<Person>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mLocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager

        //request map
        mapFragment.getMapAsync(this)


    }

    //When map is loaded
    override fun onMapReady(p0: GoogleMap) {
        p0.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(-26.167616, 28.079329), 10f))

        mClusterManager = ClusterManager(this,p0)


        p0.setOnCameraIdleListener(mClusterManager);
        p0.setOnMarkerClickListener(mClusterManager);
        p0.setOnInfoWindowClickListener(mClusterManager)
        addPersonItems()

        mClusterManager.cluster();
    }

    //Add Items to Map
    private fun addPersonItems() {
        for (i in 0..2) {
            mClusterManager.addItem(Person(-26.187616, 28.079329, "PJ", "https://twitter.com/pjapplez"))
            mClusterManager.addItem(Person(-26.207616, 28.079329, "PJ2", "https://twitter.com/pjapplez"))
            mClusterManager.addItem(Person(-26.217616, 28.079329, "PJ3", "https://twitter.com/pjapplez"))
        }
    }

}
