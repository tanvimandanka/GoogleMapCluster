package com.googlemap.model

import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem


class Person(lat: Double, lng: Double, var name: String?,  var twitterHandle: String) : ClusterItem {

    private val mPosition: LatLng

    init {
        mPosition = LatLng(lat, lng)
    }

    override fun getPosition(): LatLng {
        return mPosition
    }

    override fun getTitle(): String? {
        return name
    }

    override fun getSnippet(): String {
        return twitterHandle
    }
}